#!/usr/bin/env python

"""PuTTY Session Creator creates sessions for PuTTY SSH client based from a config file.""" 

import string
import urllib.parse
from winreg import *

__author__ = "Manuel Faux"
__copyright__ = "Copyright 2012, Manuel Faux"
__license__ = "GPL"
__version__ = "0.1"
__status__ = "Prototype"

try:
	putty_root = OpenKey(HKEY_CURRENT_USER, r'Software\SimonTatham\PuTTY\Sessions')
	# Open default session to read values
	default_session = OpenKey(putty_root, 'Default%20Settings')
except OSError:
	print("ERROR: Default session does not exist. Please save default session in PuTTY.")
	exit()


def read_config_file(config_file, *config):
	current_definition = None
	config = []
	line_no = 0
	with open(config_file) as f:
		for line in f:
			line_no += 1
			# Strip white-space from beginning and end of line
			line = line.rstrip().lstrip()
			# Ignore empty and comment lines
			if len(line) == 0 or line[0] == '#' and line[:2] != '#!':
				continue;
			# Each field is separated by semicolon
			cols = line.split(';')
			# Strip white-space of each single field
			cols = list(map(lambda x: x.lstrip().rstrip(), cols))
			# Check for column definition
			if line[:2] == '#!':
				cols[0] = cols[0][2:].lstrip()
				current_definition = parse_column_definition(cols)
				print("INFO: Changed column definition to '%s;*'." % (
					';'.join(current_definition)))
				continue
			# Check if column definition was set before first line to insert
			if current_definition == None:
				print("ERROR: No column definition found.")
				exit()
			# Check if line has enough columns
			if len(cols) < len(current_definition):
				print("WARNING: Line %d has too less arguments. Ignoring.", line_no)
			# Parse current line
			current_line = {}
			for c in range(0, len(cols)):
				# Col is defined
				if c < len(current_definition):
					current_line[current_definition[c]] = cols[c]
				# Col contains extra parameter (after defined cols)
				elif cols[c].find('=') >= 0:
					name, value = cols[c].split('=', 1)
					current_line[name] = value
			config.append(current_line)
	return config


def parse_column_definition(columns):
	col_def = []
	for col in columns:
		if len(col) == 0 or col[0] not in string.ascii_letters:
			break
		col_def.append(col)
	return col_def


def remove_all_sessions():
	keys = []
	i = 0
	# Retrieve all session names except the default session by iterating
	# trough all sessions
	while True:
		try:
			sub_key_name = EnumKey(putty_root, i)
			# Check if session is default session
			if sub_key_name != default_session_name:
				keys.append(sub_key_name)
			i += 1
		except OSError:
			break
	# Delete all retrieved sessions
	for sub_key in keys:
		DeleteKey(putty_root, sub_key)


def remove_session(name):
	try:
		DeleteKey(putty_root, name)
		print("INFO: Removed session '%s'." % name)
	except OSError:
		pass


def create_session(name, **args):
	# Prepare name
	name = urllib.parse.quote(name, safe='[]', encoding='iso-8859-1')
	# Create new (empty) session
	new_session = CreateKey(putty_root, name)
	print("INFO: Creating session '%s' with %d arguments." % (name, len(args)))
	i = 0
	# Read each value of default session and write to new session
	while True:
		try:
			value = EnumValue(default_session, i)
			SetValueEx(new_session, value[0], 0, value[2], value[1])
		except OSError:
			break
		i += 1
	# Override specified settings in new session
	for key in args.keys():
		# Skip Name attribute (only used for key name)
		if key == 'Name':
			continue
		try:
			# Read type of original value
			type = QueryValueEx(new_session, key)[1]
			# Override key concerning on the type
			if type == REG_SZ:
				SetValueEx(new_session, key, 0, REG_SZ, args[key])
			elif type == REG_DWORD:
				SetValueEx(new_session, key, 0, REG_DWORD, int(args[key]))
			print("INFO: Setting '%s' in '%s' to '%s'." % (
				key, name, args[key]))
		except ValueError:
			print("WARNING: Skipping value '%s' for '%s' in session '%s'." % (
				args[key], key, name))
		except OSError:
			print("WARNING: Value '%s' in session '%s' does not exist." % (
				key, name))


#remove_all_sessions()
config = read_config_file('input.txt')
for session in config:
	remove_session(session['Name'])
	create_session(session['Name'], **session)
